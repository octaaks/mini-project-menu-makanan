<?php

namespace App\Http\Controllers;

use App\Food;
use Illuminate\Http\Request;

class FoodController extends Controller
{
    public function index(Request $request)
    {
        $allFood = Food::all();
        return view('food.view', ['foodData' => $allFood]);
    }

    function create(Request $request)
    {
        $submit = $request->all();

        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|integer'
        ]);

        $food = new Food;
        $food->id = $submit['id'];
        $food->name = $submit['name'];
        $food->description = $submit['description'];
        $food->price = $submit['price'];
        $food->pic = $submit['pic'];
        $food->save();

        return redirect('/')->with('success', 'New data has been added!');
    }

    function edit($id)
    {
        $food = Food::find($id);
        if(!$food){
            return redirect('/')->with('error','Item with id: $id not found!');
        }
        return view('food.edit', ['foodData' => $food]);
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required'
        ]);

        $food = Food::find($id);
        if(!$food){
            return redirect('/')->with('error','Item with id: $id not found!');
        }
        
        $food -> name = $request-> name;
        $food -> description = $request-> description;
        $food -> price = $request-> price;
        $food -> pic = $request-> pic;
        $food->save();

        return redirect('/')->with('success', 'Data saved succesfully!');
    }

    function destroy(Request $request, $id)
    {
        $food = Food::find($id);
        
        if(!$food){
            return redirect('/')->with('error','Item with id: $id not found!');
        }
        $food->delete($request);
        return redirect('/')->with('success', 'Data has been deleted!');
    }
}
