<?php

namespace App\Http\Controllers;

use App\Food;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FoodApiController extends Controller
{
    public function index(){
        $food = Food::all();
        return response()->json($food);
    }
    public function create(request $request){
        
        $request->validate([
            'name' => 'required',
            'price' => 'required'
        ]);
        
        $food = new Food;
        $food -> name = $request-> name;
        $food -> description = $request-> description;
        $food -> price = $request-> price;
        $food -> pic = $request-> pic;
        $food->save();

        return response()->json(["success" => true, "message" => "data added"]);
    }
    public function update(request $request, $id){

        $request->validate([
            'name' => 'required',
            'price' => 'required'
        ]);

        $food = Food::find($id);
        if(!$food){
            return response()->json(["error" => true, "message" => "id not found"]);
        }
        
        $food -> name = $request-> name;
        $food -> description = isset($request-> description)? $request-> description : $food -> description;
        $food -> price = $request-> price;
        $food -> pic = isset($request-> pic)? $request-> pic : $food -> pic;
        $food->save();
        return response()->json(["success" => true, "message" => "updated"]);
    }
    public function destroy($id){
        $food = Food::find($id);
        if(!$food){
            return response()->json(["error" => true, "message" => "id not found"]);
        }
        $food->delete();

        return response()->json(["success" => true, "message" => "deleted"]);
    }
}
