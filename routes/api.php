<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/view', 'FoodController@apiView');
// Route::post('/add', 'FoodController@apiAdd');
// Route::post('/update/{id}', 'FoodController@apiUpdate');
// Route::get('/delete/{id}', 'FoodController@apiDelete');

Route::name('api.')->prefix('')->group(function(){
    Route::name('index')->get('list','FoodApiController@index');
    Route::name('create')->post('create','FoodApiController@create');
    Route::name('update')->post('{id}','FoodApiController@update');
    Route::name('destroy')->get('{id}','FoodApiController@destroy');
});