<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', 'FoodController@index');
// Route::post('/create', 'FoodController@create');
// Route::get('/edit/{id}', 'FoodController@edit');
// Route::post('/update/{id}', 'FoodController@update');
// Route::get('/destroy/{id}', 'FoodController@destroy');

Route::name('food.')->prefix('')->group(function(){
    Route::name('index')->get('','FoodController@index');
    Route::name('create')->post('create','FoodController@create');
    Route::name('edit')->get('{id}/edit','FoodController@edit');
    Route::name('update')->post('{id}','FoodController@update');
    Route::name('destroy')->get('{id}','FoodController@destroy');
});