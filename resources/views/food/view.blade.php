
<html>

<head>
    <title></title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="mt-5">
        <div class="mx-auto" style="width: 90%;">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">
                Add Menu
            </button>
            <br>
            <br>
            @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{session('success')}}
            </div>
            @endif
            <table class="table table-bordered table-sm table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>DESCRIPTION</th>
                        <th>PRICE</th>
                        <th>PIC</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($foodData as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->description}}</td>
                        <td>{{$item->price}}</td>
                        <td><center><img src="/gambar/{{$item->pic}}" width="128" height="100"></center></td>
                        <td width="20%">
                            <a class="btn btn-primary  btn-sm" href="{{$item->id}}/edit/" role="button">Edit</a>
                            <a class="btn btn-danger  btn-sm" method="delete" href="{{ route('food.destroy', ["id" => $item->id]) }}" role="button">Delete</a>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

            <!-- Modal TAMBAH -->
            <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Menu</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="post" action="/create">
                                {{ csrf_field() }}
                                <div class="">
                                    <table class="table table-borderless  table-sm">
                                        <tr>
                                            <input type="hidden" name="id" required="">
                                        </tr>
                                        <!-- <tr>
                                            <td>ID</td>
                                            <td><input class="form-control" type="text" placeholder="Food ID" name="id"></td>
                                        </tr> -->
                                        <td>Name</td>
                                        <td>
                                            <input class="form-control" type="text" placeholder="Food name" name="name">
                                        </td>
                                        </tr>
                                        <tr>
                                            <td>Descsription</td>
                                            <td><input class="form-control" type="text" placeholder="Food description" name="description"></td>

                                        </tr>
                                        <tr>
                                            <td>Price</td>
                                            <td><input class="form-control" type="text" placeholder="Food price" name="price"></td>

                                        </tr>
                                        <tr>
                                            <td>Picture</td>
                                            <td><input class="form-control" type="text" placeholder="Pic file" name="pic">

                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <center><input class="btn btn-primary btn-lg btn-block" type="submit" value="Insert">
                                            </td>
                                        </tr>
                                    </table>
                            </form>
                        </div>
                        <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
</body>

</html>