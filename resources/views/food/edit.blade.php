<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<html>

<head>
</head>

<body>
	<form method="post" action="/{{$foodData->id}}">
		{{ csrf_field() }}
		<div class="mt-5">
			<div class="mx-auto" style="width: 90%;">
				<table class="table table-borderless">
					<tr>
						<input type="hidden" name="id" value="{{ $foodData->id }}">
					</tr>
					<!-- <tr>
						<td>ID</td>
						<td><input class="form-control" type="text" placeholder="ID" name="id" value="{{ $foodData->id }}"></td>
					</tr> -->
					<tr>
						<td>Name</td>
						<td><input class="form-control" type="text" placeholder="Name" name="name" value="{{ $foodData->name }}"></td>

					</tr>
					<tr>
						<td>Description</td>
						<td><input class="form-control" type="text" placeholder="Description" name="description" value="{{ $foodData->description }}"></td>

					</tr>
					<tr>
						<td>Price</td>
						<td><input class="form-control" type="text" placeholder="Price" name="price" value="{{ $foodData->price }}"></td>

					</tr>
					<tr>
						<td>Picture</td>
						<td><input class="form-control" type="text" placeholder="pic" name="pic" value="{{ $foodData->pic }}"></td>

					</tr>
					<tr>
						<td colspan="2">
							<center><input class="btn btn-primary btn-lg btn-block" type="submit" value="Save">
						</td>

					</tr>
				</table>
	</form>
</body>

</html>